import React, { Component } from 'react';
import { connect } from 'react-redux';
import Template from '../components/Template/template';
import { css } from 'aphrodite/no-important';
import styles from './styles';
import { getTemplate } from  '../actions/global';
import Error from '../components/Error/error';
import ButtonGroup from '../components/ButtonGroup/buttonGroup';

const mapStateToProps = state => state;

const mapDispatchToProps = {
    getTemplate,
};

class App extends Component {
    state = {
        template: null,
    };

    static getDerivedStateFromProps(props, state) {
        if (props.global.templates[0] && !state.template) {
            return {
                ...state,
                template: props.global.templates[0].template,
            };
        }
        return null;
    }

    componentDidMount() {
        this.props.getTemplate();
    };

    onChangeTemplate = (event) => {
        const value = event.target.value;
        const configure = this.props.global.templates[value].template;
        this.setState({ template: configure });
    };

  render() {
      const { global: { templates, properties, errorMessage } } = this.props;
      const { template } = this.state;
      return (
      <div className={ css(styles.page) }>
          <ButtonGroup configure={templates} onChange={ this.onChangeTemplate } />
          {properties[0] && template &&
          <div className={ css(styles.content) } >
              {properties.map((property, index)=> (
                  <div className={ css(styles.template) } key={ index } >
                      <Template configure={ template } property={ property } />
                  </div>
              ))}
          </div>
          }
          {!properties[0] && errorMessage &&
          <Error errorMessage={ errorMessage } className={ css(styles.content) } />
          }
      </div>
    );
  }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)

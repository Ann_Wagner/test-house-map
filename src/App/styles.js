import { StyleSheet } from 'aphrodite/no-important';
import {colors, mediaQueries} from "../styles/variables";

export default StyleSheet.create({
    page: {
        padding: '20px',
        [mediaQueries.large]: {
            padding: '60px',
        }
    },

    content: {
        display: 'block',
        clear: 'both',
        marginTop: '100px',
        [mediaQueries.large]: {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
        }
    },

    template: {
        flex: 1,
        marginBottom: '20px',
        padding: '20px',
        borderStyle: 'solid',
        borderColor: colors.lightGrey,
        borderWidth: '1px',
        boxShadow: '0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12)',
        [mediaQueries.large]: {
            flex: '0 47%',
        }
    },

    error: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
    },
});

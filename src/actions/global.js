import { GET_TEMPLATE, GET_PROPERTIES } from '../constants/actions/global';

export const getTemplate = () => ({
    type: GET_TEMPLATE.START,
});

export const getTemplateSuccess = data => ({
    type: GET_TEMPLATE.SUCCESS,
    payload: data,
});

export const getTemplateError = data => ({
    type: GET_TEMPLATE.ERROR,
    payload: data,
});

export const getProperties = () => ({
    type: GET_PROPERTIES.START,
});

export const getPropertiesSuccess = data => ({
    type: GET_PROPERTIES.SUCCESS,
    payload: data,
});

export const getPropertiesError = data => ({
    type: GET_PROPERTIES.ERROR,
    payload: data,
});

import React, { Component } from 'react';
import {connect} from "react-redux";
import { css } from 'aphrodite/no-important';
import styles from './styles';

const mapStateToProps = state => (state);

class Address extends Component {
    render() {
        const { address } = this.props;
        return (
            <div className={ css(styles.address) }>{address}</div>
        );
    }
}

export default connect(
    mapStateToProps,
)(Address)

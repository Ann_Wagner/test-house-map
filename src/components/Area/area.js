import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite/no-important';
import styles from './styles'

const mapStateToProps = state => (state);

class Area extends Component {

    render() {
        const { area } = this.props;
        return (
            <div className={ css(styles.area) }>{area} sq.fr.</div>
        );
    }
}

export default connect(
    mapStateToProps,
)(Area)

import { StyleSheet } from 'aphrodite/no-important';

export default StyleSheet.create({
    area: {
        marginTop: '40px',
        marginBottom: '10px',
    }
});

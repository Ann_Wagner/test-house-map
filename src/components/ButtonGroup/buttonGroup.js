import React, { Component } from 'react';
import {connect} from "react-redux";
import './styles.scss';

const mapStateToProps = state => (state);

class ButtonGroup extends Component {
    render() {
        const { configure, onChange } = this.props;
        return (
            <div className="buttons">
                {configure[0] && configure.map((el, index) => (
                    <label><input type="radio" name="toggle" value={index} onChange={ onChange } /><span>Template {index +1}</span></label>
                ))}
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(ButtonGroup)

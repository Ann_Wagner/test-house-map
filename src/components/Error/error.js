import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite/no-important';
import styles from './styles'

const mapStateToProps = state => (state);

class Error extends Component {
    render() {
        const { errorMessage } = this.props;
        return (
            <div className={ css(styles.error) } >
                <div>Something went wrong</div>
                <div className={ css(styles.message) } > {errorMessage } </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(Error)

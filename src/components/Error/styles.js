import { StyleSheet } from 'aphrodite/no-important';
import { colors } from '../../styles/variables';

export default StyleSheet.create({
    error: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100vh',
        textTransform: 'uppercase',
        fontWeight: 'bold',
        fontSize: '20px',
        color: colors.red,
    },

    message: {
        textTransform: 'capitalize',
    },
});

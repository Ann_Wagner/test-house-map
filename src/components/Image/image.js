import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite/no-important';
import styles from './styles';
import Template from "../Template/template";

const mapStateToProps = state => (state);

class Image extends Component {

    render() {
        const { images, children, property } = this.props;
        return (
            <section className={ css(styles.container) } >
                {images[0] &&
                    images.map((image, index) => (
                        <img src={image} alt="home" className={ css(styles.image) } key={index} />
                    ))
                }
                {
                    children &&
                    <div className={ css(styles.template) }>
                        <Template configure={ children } property={ property } />
                    </div>
                }
            </section>
        );
    }
}

export default connect(
    mapStateToProps,
)(Image)

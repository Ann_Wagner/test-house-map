import { StyleSheet } from 'aphrodite/no-important';
import { colors, mediaQueries } from '../../styles/variables';

export default StyleSheet.create({
    container: {
        display: 'flex',
        flexFlow: 'row wrap',
        position: 'relative',
        widths: '100%',
    },

    image: {
        width: '100%',
        height: 'auto',
        objectFit: 'cover',
        filter: 'brightness(60%)',
        [mediaQueries.medium]: {
            width: '200px',
            height: '200px',
        },
        [mediaQueries.large]: {
            width: '250px',
            height: '250px',
        },
    },

    template: {
        position: 'absolute',
        left: '5%',
        fontSize: '18px',
        fontWeight: 500,
        color: colors.darkWhite,
    }
});

import React, { Component} from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite/no-important';
import styles from './styles';
import { normalizeCurrency } from '../../utils/normalize'

const mapStateToProps = state => (state);
class Price extends Component {
    render() {
        const { price} = this.props;
        return (
            <div className={ css(styles.price) }>
                { normalizeCurrency(price) }
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(Price)

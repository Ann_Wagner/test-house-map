import { StyleSheet } from 'aphrodite/no-important';

export default StyleSheet.create({
    price: {
        margin: '10px auto',
        fontWeight: 'bold',
        fontSize: '1.6rem',
    },

    margin: {
        marginTop: '40px',
    }
});

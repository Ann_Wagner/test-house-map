import React, { Component } from 'react';
import { connect } from 'react-redux';
import Address from '../Address/address';
import Area from '../Area/area';
import Price from '../Price/price';
import Image from '../Image/image';

const mapStateToProps = state => state;

class Template extends Component {
    render() {
        const { property, configure } = this.props;
        return (
            <>
                {configure && configure.map((item, index)=> {
                    switch (item.component) {
                        case 'ADDRESS': {
                            return property[item.field] ?
                                <Address address={property[item.field]} key={ index } /> : null;
                        }
                        case 'IMAGE': {
                            return property[item.field] ?
                                <Image images={property[item.field]} property={property}
                                       children={item.children}
                                       key={ index } /> : null;
                        }
                        case 'AREA': {
                            return property[item.field] ?
                                <Area area={property[item.field]} key={ index } /> : null;
                        }
                        case 'PRICE': {
                            return property[item.field] ?
                                <Price price={property[item.field]}  key={ index } /> : null;
                        }
                        default: {
                            return null;
                        }
                    }
                })}
            </>
        );
    }
}

export default connect(
    mapStateToProps,
)(Template)

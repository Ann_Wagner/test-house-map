import { defineActionConstants } from '../../utils/defineActionConstants';
import { START, SUCCESS, ERROR } from '../actionsSubLevels';

export const GET_PROPERTIES = defineActionConstants('GET_PROPERTIES', [START, SUCCESS, ERROR]);

export const GET_TEMPLATE = defineActionConstants('GET_TEMPLATE', [START, SUCCESS, ERROR]);

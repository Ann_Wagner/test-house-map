import { GET_TEMPLATE, GET_PROPERTIES } from '../constants/actions/global';

const initialState = {
    templates: [],
    properties: [],
    errorMessage: null,
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case GET_TEMPLATE.SUCCESS: {
            return {
                ...state,
                templates: action.payload
            };
        }
        case GET_TEMPLATE.ERROR: {
            return {
                ...state,
                errorMessage: action.payload
            };
        }
        case GET_PROPERTIES.SUCCESS: {
            return {
                ...state,
                properties: action.payload
            };
        }
        case GET_PROPERTIES.ERROR: {
            return {
                ...state,
                errorMessage: action.payload
            };
        }
        default:
            return state;
    }
}

export default reducer;

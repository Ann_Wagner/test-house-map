import { takeEvery, call, put } from 'redux-saga/effects';
import { getTemplateSuccess, getProperties, getPropertiesSuccess, getTemplateError, getPropertiesError } from '../actions/global'

import { GET_TEMPLATE, GET_PROPERTIES } from '../constants/actions/global';
import api from '../utils/api/api';

function* requestTemplates() {
    try {
        const response = yield call(api.get, '/templates');
        yield put(getTemplateSuccess(response.data));
        yield put(getProperties());
    } catch (error) {
        yield put (getTemplateError(error.message))
    }
}

function* requestProperties() {
    try {
        const response = yield call(api.get, '/properties');
        yield put(getPropertiesSuccess(response.data.data));
    } catch (error) {
        yield put (getPropertiesError(error.message))
    }
}
export default function* watchSaga() {
    yield takeEvery(GET_TEMPLATE.START, requestTemplates);
    yield takeEvery(GET_PROPERTIES.START, requestProperties);
}


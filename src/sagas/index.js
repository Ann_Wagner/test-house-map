import { all, fork } from 'redux-saga/effects';

import globalSaga from './global';

const sagas = [
    globalSaga,
];

export default function* root() {
    yield all(sagas.map(saga => fork(saga)));
}

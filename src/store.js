import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';

import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = (middleware) => {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
};

const configureStore = () => {
    const store = createStore(
        rootReducer,
        bindMiddleware([sagaMiddleware]),
    );

    store.runSaga = sagaMiddleware.run;

    store.close = () => store.dispatch(END);

    sagaMiddleware.run(rootSaga);

    return store;
};
export default configureStore;

export const colors = {
    darkWhite: '#EAEAEC',
    lightGrey: '#D3D3D3',
    grey: '#808080',
    dimGrey: '#696969',
    red: '#F62121',
};

export const mediumWidth = 768;
export const largeWidth = 1200;

export const mediaQueries = {
    small: '@media only screen and (min-width: 640px)',
    medium: `@media only screen and (min-width: ${mediumWidth}px)`,
    large: `@media only screen and (min-width: ${largeWidth}px)`,
};

const NAMESPACE_SEPARATOR = '/';

export default (type, subactions = [], namespace) => {
    if (subactions && subactions.ACTION || typeof subactions === 'string') {
        namespace = subactions;
    }

    if (!Array.isArray(subactions)) {
        subactions = [];
    }

    const name = (namespace) ? [namespace, type].join(NAMESPACE_SEPARATOR) : type;

    const action = subactions.reduce((r, i) => Object.assign({}, r, {
        [i]: `${name}_${i}`,
    }), {});

    action.ACTION = name;

    action.toString = () => name.toString();
    return action;
};


export const normalizeCurrency = (value, currency) => {
    currency = currency ? currency : '$';
    if (!value) {
        return value;
    }
    return currency + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ');
};
